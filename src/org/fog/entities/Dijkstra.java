package org.fog.entities;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Dijkstra {
	private double dist[];
    private Set<Integer> visited;
    private Map<Integer,FogDevice> devices; // Number of vertices 

    
    public Dijkstra(Map<Integer,FogDevice> devices, FogDevice mobile) 
    { 
        this.devices = devices; 
        dist = new double[devices.size()]; 
        visited = new HashSet<>();
        algDijkstra(devices, mobile);
    }
    
    private void algDijkstra(Map<Integer,FogDevice> lista, FogDevice fog) {
    	for (int i= 0; i < devices.size(); i++) {
    		dist[i] = Integer.MAX_VALUE;
    	}
    	dist[fog.getId()] = 0;
    	for (Integer i: fog.getNeighbors().keySet()) {
    		if (i != -1)
    			dist[i] = fog.getNeighbors().get(i);
    	}
    	while(true) {
    		int nodeMinCost = findMin(dist, visited);
    		if (nodeMinCost == -1) return;
    		visited.add(nodeMinCost);
    		e_Neighbours(lista.get(nodeMinCost));
//    		System.out.println(settled);
    	}
    	
    }
    
    private void e_Neighbours(FogDevice fog) {
    	double edgeDistance = -1; 
        double newDistance = -1;
        for (Integer figlio : fog.getNeighbors().keySet()) {
        	if (figlio != -1) {
	        	FogDevice neib = this.devices.get(figlio);
	        	// If current node hasn't already been processed
    			edgeDistance = fog.getNeighbors().get(neib.getId()); 
    			newDistance = this.dist[fog.getId()] + edgeDistance; 
        		// If new distance is cheaper in cost 
        		if (newDistance < this.dist[neib.getId()]) 
        			this.dist[neib.getId()] = newDistance; 
        	}
        }
    }
    
    public double[] getDistance() {
    	return dist;
    }
    
    public int findMin(double[] dist, Set<Integer> visited){
    	int next = -1;
    	double costoMin = Double.MAX_VALUE;
    	for(int node=0; node < dist.length; node++) {
    		if (!visited.contains(node) && dist[node] < costoMin) {
    			next = node;
    			costoMin = dist[node];
    		}
    	}
    	return next;
    }
    
    @Override
    public String toString() {
    	return Arrays.toString(dist);
    }
	 
}
