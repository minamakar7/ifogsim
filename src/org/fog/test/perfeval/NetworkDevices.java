package org.fog.test.perfeval;

import java.util.TreeMap;

import org.fog.entities.Actuator;
import org.fog.entities.FogDevice;
import org.fog.entities.Sensor;

public class NetworkDevices {
	private Sensor sensore;
	private Actuator display;
//	private FogDevice mobile;
//	private FogDevice gateway;
//	private FogDevice proxy;
//	private FogDevice cloud;
	private TreeMap<Integer,FogDevice> fogs;
	
	
	public NetworkDevices(Sensor sensore, Actuator display,TreeMap<Integer,FogDevice> fogs) {
//			,FogDevice mobile,FogDevice gateway, FogDevice proxy,FogDevice cloud) {
		this.sensore = sensore;
		this.display = display;
//		this.mobile = mobile;
//		this.gateway = gateway;
//		this.proxy = proxy;
//		this.cloud = cloud;
		//cloud,proxy,gateway,
//		fog = List.of(cloud,proxy,gateway,mobile);
		this.fogs = fogs;
	}
	
	public Sensor getSensore() {
		return sensore;
	}

	public Actuator getDisplay() {
		return display;
	}

//	public FogDevice getMobile() {
//		return mobile;
//	}
//
//	public FogDevice getGateway() {
//		return gateway;
//	}
//
//	public FogDevice getProxy() {
//		return proxy;
//	}
//
//	public FogDevice getCloud() {
//		return cloud;
//	}
//	
	public TreeMap<Integer,FogDevice> getDevices(){
		return fogs;
	}
	
	@Override
	public String toString() {
		return "Sensore:" + getSensore().getName();
	}

}
