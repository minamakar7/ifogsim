package org.fog.test.perfeval;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiConsumer;

import org.fog.application.Application;
import org.fog.entities.FogDevice;

import com.google.common.base.Function;

public class Utils {
	public static TreeMap<Integer,FogDevice> fileToDevice(String file, Function<String[],FogDevice> f){
		TreeMap<Integer,FogDevice> fogs = new TreeMap<>();
		List<String[]> lines = readFile(file);
		for (String[] line: lines) {
			FogDevice fog = f.apply(line);
			fogs.put(fog.getId(),fog);
		}
		return fogs;
	}
	
	public static void addAppmodule(String file,Application app, BiConsumer<Application, String[]> cons) {
		List<String[]> lines = readFile(file);
		for (String[] line: lines) {
			cons.accept(app, line);
		}
	}
	
	@SuppressWarnings("resource")
	public static List<String[]> readFile(String file) {
		List<String[]> lista = new ArrayList<>();
		try {
			FileReader filereader = new FileReader(file);
			BufferedReader csv = new BufferedReader(filereader);
			String line= csv.readLine();
			 while ((line = csv.readLine()) != null) {
				 lista.add(line.split(";"));
			 }
			 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return lista;
	}
}
