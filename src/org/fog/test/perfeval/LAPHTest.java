package org.fog.test.perfeval;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;
import org.cloudbus.cloudsim.sdn.overbooking.BwProvisionerOverbooking;
import org.cloudbus.cloudsim.sdn.overbooking.PeProvisionerOverbooking;
import org.fog.application.Application;
import org.fog.entities.Actuator;
import org.fog.entities.Dijkstra;
import org.fog.entities.FogBroker;
import org.fog.entities.FogDevice;
import org.fog.entities.FogDeviceCharacteristics;
import org.fog.entities.Sensor;
import org.fog.placement.LAPH;
import org.fog.placement.LAPH2;
import org.fog.placement.ModuleMapping;
import org.fog.policy.AppModuleAllocationPolicy;
import org.fog.scheduler.StreamOperatorScheduler;
import org.fog.utils.FogLinearPowerModel;
import org.fog.utils.FogUtils;
import org.fog.utils.distribution.DeterministicDistribution;

public class LAPHTest {
	public static void main(String[] args) {
		Log.printLine("Starting LAPH...");
		try {
			//Log.disable();
			Log.disable();
			int num_user = 1; // number of cloud users
			Calendar calendar = Calendar.getInstance();
			boolean trace_flag = false; // mean trace events

			CloudSim.init(num_user, calendar, trace_flag);

			String appId = "LAPH1"; // identifier of the application
			
			FogBroker broker = new FogBroker("broker");

			NetworkDevices networkDevices = createFogDevices(broker.getId(), "LAPH","./1 serie 24.csv");
			startApp(networkDevices, broker);
			
			//System.out.println(listaFog);

//			Controller controller = new Controller("master-controller", fogs, sensori, displays);
			
//			controller.submitApplication(application1, laph);
			
//			controller.submitApplication(application, laph);
//			TimeKeeper.getInstance().setSimulationStartTime(Calendar.getInstance().getTimeInMillis());
//			CloudSim.startSimulation();
//			CloudSim.stopSimulation();
//			Log.printLine("LAPH finished!");
		}
		catch(Exception e){
			e.printStackTrace();
			Log.printLine("errore");
		}
	}
	
	public static void startApp(NetworkDevices networkDevices, FogBroker broker) throws IOException {
		List<Application> apps = createDataSet();
//		apps.sort(Comparator.comparingInt(Application::getStartTime));
		ModuleMapping moduleMapping = ModuleMapping.createModuleMapping();
		List<Sensor> sensori = new ArrayList<>();
		sensori.add(networkDevices.getSensore());
		List<Actuator> displays = new ArrayList<>();
		displays.add(networkDevices.getDisplay());
		List<FogDevice> fogs = new ArrayList<>();
				fogs.addAll(networkDevices.getDevices().values());
		double[][] latency = new double[networkDevices.getDevices().size()][networkDevices.getDevices().size()];
		for (Integer i: networkDevices.getDevices().keySet()) {
			latency[i] = new Dijkstra(networkDevices.getDevices(), networkDevices.getDevices().get(i)).getDistance();
		}
		LAPH laph = new LAPH(networkDevices,apps.get(0),moduleMapping,latency);
		simulation(apps,laph);
	}
	
	public static List<Application> createDataSet() {
		int k = 5;
		int minreq=1;
//		List<Double> nr_periodic = new ArrayList<>();
//		List<Double> nr_steps = new ArrayList<>();
//		List<Double> nr_quad = new ArrayList<>();
		int maxreq=10;
		int shape=500;
//		double shape2=0.001;
//		double shape3=0.0005;
		List<Application> apps = new ArrayList<>();
		int numApp = 0;
		for (int i = 0; i<= 1000; i += k ) {
			Random rand = new Random();
			double numAppTime = (int) Math.ceil((maxreq-minreq)*(1-Math.sin(2*i/shape + Math.PI/2))/2) + 1;
			for (int np=0; np < numAppTime; np++) {
				int n = rand.nextInt(10) + 1;
				numApp++;
				apps.add(createApplication("LAPH"+ numApp, numApp,i,n));
				int n2 = rand.nextInt(n) + i;
				apps.add(createApplication("LAPH"+ numApp, numApp,n2,n));
			}
//			nr_steps.add(minreq+Math.ceil(shape2*i));
//			nr_quad.add(minreq+Math.ceil((shape3*i)*(shape3*i)));
		}
//		apps.add(createApplication("LAPH"+ 1, 1,1,1));
//		apps.add(createApplication("LAPH"+ 2, 2,2,2));
//		apps.add(createApplication("LAPH"+ 3, 3,3,2));
//		apps.add(createApplication("LAPH"+ 4, 4,4,1));
//		apps.add(createApplication("LAPH"+ 5, 5,5,1));
//		apps.add(createApplication("LAPH"+ 6, 6,6,1));
		return apps;
	}
	
	private static void simulation(List<Application> apps, LAPH laph) throws IOException {
		int t = 0;
		int i = 0;
		//FileWriter file = new FileWriter("file2.txt");
		while (i < apps.size()) {
			
			if (apps.get(i).getStartTime() == t) {
//				if (i%100 == 0) {
//					file.write("sto analizzando "+ apps.get(i)+ "\n");
//				}
				laph.setApplication(apps.get(i));
				laph.mapModules();
				i++;
			}
			else {
				laph.taskControl();
				t++;
			}
		}
		//file.close();
		System.out.println(laph.getModuleToDeviceMap());
		System.out.println(laph.getDeviceToModuleMap());
//		for (Integer fog: laph.getDeviceToModuleMap().keySet()) {
//			List<AppModule> mods = laph.getDeviceToModuleMap().get(fog);
//			for (AppModule mod: mods) System.out.println("Sono mod: "+ mod+ " e sono in:"+" mi trovo nel "+ fog+ " place:" +mod.getDevicePlace());
//			//laph.getMyStruct().getDevices().get(fog).get;
//		}
	}
	
	private static NetworkDevices createFogDevices(int userId, String appId,String dir) {
		Sensor sensor = new Sensor("s-"+appId, "sensore", userId, appId, new DeterministicDistribution(5.1));
		sensor.setGatewayDeviceId(7);
		sensor.setLatency(3.0);
		
		Actuator display = new Actuator("a-"+appId, userId, appId, "display");
		display.setGatewayDeviceId(7);
		display.setLatency(3.0);
		TreeMap<Integer,FogDevice> listaFog = Utils.fileToDevice(dir,(i)->createFogDevice(i));
		return new NetworkDevices(sensor,display,listaFog);
	}
	
	private static FogDevice createFogDevice(String nodeName, long mips,
			int ram, int upBw, long downBw, int level, double ratePerMips, double busyPower,
			double idlePower,int id, int idParent, double latency) {
		List<Pe> peList = new ArrayList<Pe>();
		// 3. Create PEs and add these into a list.
		peList.add(new Pe(0, new PeProvisionerOverbooking(mips))); // need to store Pe id and MIPS Rating

		int hostId = FogUtils.generateEntityId();
		long storage = 1000000; // host storage
		//era 10000
		int bw = upBw;

		PowerHost host = new PowerHost(
				hostId,
				new RamProvisionerSimple(ram),
				new BwProvisionerOverbooking(bw),
				storage,
				peList,
				new StreamOperatorScheduler(peList),
				new FogLinearPowerModel(busyPower, idlePower)
			);

		List<Host> hostList = new ArrayList<Host>();
		hostList.add(host);

		String arch = "x86"; // system architecture
		String os = "Linux"; // operating system
		String vmm = "Xen";
		double time_zone = 10.0; // time zone this resource located
		double cost = 3.0; // the cost of using processing in this resource
		double costPerMem = 0.05; // the cost of using memory in this resource
		double costPerStorage = 0.001; // the cost of using storage in this
										// resource
		double costPerBw = 0.0; // the cost of using bw in this resource
		LinkedList<Storage> storageList = new LinkedList<Storage>(); // we are not adding SAN
													// devices by now

		FogDeviceCharacteristics characteristics = new FogDeviceCharacteristics(
				arch, os, vmm, host, time_zone, cost, costPerMem,
				costPerStorage, costPerBw);

		FogDevice fogdevice = null;
		try {
			fogdevice = new FogDevice(nodeName, characteristics, 
					new AppModuleAllocationPolicy(hostList), storageList, 10, upBw, downBw, 0, ratePerMips);
			fogdevice.setId(id);
			fogdevice.setParentId(idParent);
			fogdevice.setUplinkLatency(latency);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		fogdevice.setLevel(level);
		return fogdevice;
	}
	@SuppressWarnings({"serial" })
	public static Application createApplication(String appId, int userId,int startTime, int taskTime){
		Application application = Application.createApplication(appId, userId);
		Utils.addAppmodule("./module.csv", application, (app, i) -> appModules(app,i,14,7));
//		application.addAppEdge("sensore", "Module1", 3000, 500, "sensore", Tuple.UP, AppEdge.SENSOR);
//		application.addAppEdge("Module1", "Module2", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
//		application.addAppEdge("Module2", "Module3", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
//		application.addAppEdge("Module3", "Module4", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
//		application.addAppEdge("Module4", "Module5", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
//		application.addAppEdge("Module5", "Module6", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
//		application.addAppEdge("Module1", "Module6", 3500, 500, "dati", Tuple.DOWN, AppEdge.MODULE);
//		application.addAppEdge("Module1", "display", 3500, 500, "output", Tuple.DOWN, AppEdge.ACTUATOR);
//		
//		application.addAppEdge("sensore", "Module7", 3500, 500, "sensore", Tuple.UP, AppEdge.SENSOR);
//		application.addAppEdge("Module7", "display", 3500, 500, "sensore", Tuple.DOWN, AppEdge.ACTUATOR);
//		
//		application.addTupleMapping("Module1", "sensore", "dati", new FractionalSelectivity(1.0));
//		application.addTupleMapping("Module1", "dati", "output", new FractionalSelectivity(1.0));
//		application.addTupleMapping("Module2", "dati", "dati", new FractionalSelectivity(1.0));
//		application.addTupleMapping("Module3", "dati", "dati", new FractionalSelectivity(1.0));
//		application.addTupleMapping("Module4", "dati", "dati", new FractionalSelectivity(1.0));
//		application.addTupleMapping("Module5", "dati", "dati", new FractionalSelectivity(1.0));
//		application.addTupleMapping("Module6", "dati", "dati", new FractionalSelectivity(1.0));
//		
//		application.addTupleMapping("Module7", "sensore", "dati", new FractionalSelectivity(1.0));
//		application.addTupleMapping("Module7", "dati", "output", new FractionalSelectivity(1.0));
		
//		final AppLoop loop1 = new AppLoop(new ArrayList<String>(){{add("Module1");add("Module2");add("Module3");add("Module4");add("Module5");add("Module6");}});
//		final AppLoop loop2 = new AppLoop(new ArrayList<String>(){{add("Module7");}});
//		List<AppLoop> loops = new ArrayList<AppLoop>(){{add(loop1);add(loop2);}};
		
//		application.setLoops(loops);
		application.setStartTime(startTime);
		application.setTaskTime(taskTime);
		return application;
	}
	public static FogDevice createFogDevice(String[] params) {
		FogDevice fog = createFogDevice(params[0], Long.parseLong(params[1]), 
				Integer.parseInt(params[2]), Integer.parseInt(params[3]), 
				Integer.parseInt(params[4]), Integer.parseInt(params[5]), 
				Double.parseDouble(params[6]), Double.parseDouble(params[7]), 
				Double.parseDouble(params[8]), Integer.parseInt(params[9]),
				Integer.parseInt(params[10]),Double.parseDouble(params[11]));
		if (fog.getParentId() != -1)
			fog.addNeighbors(fog.getParentId(), fog.getUplinkLatency());
		if (params.length > 12) 
			for(int i=12; i<params.length; i+=2)
				if (params[i].length() > 0)
					fog.addNeighbors(Integer.parseInt(params[i]), Double.parseDouble(params[i+1]));
		return fog;
	}
	
	public static void appModules(Application app,String[] string, int maxIdMobile, int minIDMobile) {
		//int n = random.nextInt(maxIdMobile - minIDMobile + 1) + minIDMobile;
		//System.out.println(n);
		 app.addAppModule(string[0], Integer.parseInt(string[1]), Long.parseLong(string[2]), 
				Integer.parseInt(string[3]), Integer.parseInt(string[4]), string[5].equals("1"), Integer.parseInt(string[6]));
	}
	
}
