package org.fog.test.perfeval;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;
import org.cloudbus.cloudsim.sdn.overbooking.BwProvisionerOverbooking;
import org.cloudbus.cloudsim.sdn.overbooking.PeProvisionerOverbooking;
import org.fog.application.AppEdge;
import org.fog.application.AppLoop;
import org.fog.application.Application;
import org.fog.application.selectivity.FractionalSelectivity;
import org.fog.entities.Actuator;
import org.fog.entities.FogBroker;
import org.fog.entities.FogDevice;
import org.fog.entities.FogDeviceCharacteristics;
import org.fog.entities.Sensor;
import org.fog.entities.Tuple;
import org.fog.placement.Controller;
import org.fog.placement.LAPH;
import org.fog.placement.ModuleMapping;
import org.fog.placement.ModulePlacementEdgewards;
import org.fog.placement.ModulePlacementMapping;
import org.fog.policy.AppModuleAllocationPolicy;
import org.fog.scheduler.StreamOperatorScheduler;
import org.fog.utils.FogLinearPowerModel;
import org.fog.utils.FogUtils;
import org.fog.utils.TimeKeeper;
import org.fog.utils.distribution.DeterministicDistribution;

public class Test {
	public static void main(String[] args) {
		Log.printLine("Starting LAPH...");
		try {
			Log.disable();
			
			int num_user = 1; // number of cloud users
			Calendar calendar = Calendar.getInstance();
			boolean trace_flag = false; // mean trace events

			CloudSim.init(num_user, calendar, trace_flag);

			String appId = "LAPH"; // identifier of the application
			
			FogBroker broker = new FogBroker("broker");
			Application application = createApplication(appId, broker.getId());
			application.setUserId(broker.getId());
			
			NetworkDevices mystruct = createFogDevices(broker.getId(), appId);
			ModuleMapping moduleMapping = ModuleMapping.createModuleMapping();
			
			moduleMapping.addModuleToDevice("Module1", mystruct.getMobile().getName());
			moduleMapping.addModuleToDevice("Module2", mystruct.getGateway().getName());
			moduleMapping.addModuleToDevice("Module3", mystruct.getGateway().getName());
			moduleMapping.addModuleToDevice("Module4", mystruct.getProxy().getName());
			moduleMapping.addModuleToDevice("Module5", mystruct.getProxy().getName());
			moduleMapping.addModuleToDevice("Module6", mystruct.getCloud().getName());
			
			List<Sensor> sensori = new ArrayList<>();sensori.add(mystruct.getSensore());
			List<Actuator> displays = new ArrayList<>();displays.add(mystruct.getDisplay());
			Controller controller = new Controller("master-controller", mystruct.getDevices(), sensori, displays);
			controller.submitApplication(application, new ModulePlacementEdgewards(mystruct.getDevices(),sensori,displays,application, moduleMapping));
			//controller.submitApplication(application, new ModulePlacementMapping(mystruct.getDevices(), application, moduleMapping));
			TimeKeeper.getInstance().setSimulationStartTime(Calendar.getInstance().getTimeInMillis());
			CloudSim.startSimulation();
			//System.out.println("ciao");
			CloudSim.stopSimulation();
			Log.printLine("LAPH finished!");
		}
		catch(Exception e){
			e.printStackTrace();
			Log.printLine("errore");
		}
	}
	private static MyStruct createFogDevices(int userId, String appId) {
		
		FogDevice cloud = createFogDevice("cloud", 40000, 40000, 100, 10000, 0, 0.01, 16*103, 16*83.25); // creates the fog device Cloud at the apex of the hierarchy with level=0
		cloud.setParentId(-1);
		
		FogDevice proxy = createFogDevice("proxy", 15000, 8000, 10000, 10000, 3, 0.01, 16*103, 16*83.25);
		proxy.setParentId(cloud.getId());
		proxy.setUplinkLatency(2);
		
		FogDevice gateway = createFogDevice("gateway", 8000, 6000, 10000, 10000, 2, 0.01, 16*103, 16*83.25);
		gateway.setParentId(proxy.getId());
		gateway.setUplinkLatency(2);
		
		FogDevice mobile = createFogDevice("mobile", 4000, 2000, 250, 250, 1, 0.01, 16*103, 16*83.25);
		mobile.setParentId(gateway.getId());
		mobile.setUplinkLatency(2);
		
		Sensor sensor = new Sensor("s-"+appId, "sensore", userId, appId, new DeterministicDistribution(5.1));
		sensor.setGatewayDeviceId(mobile.getId());
		sensor.setLatency(13.0);
		
		Actuator display = new Actuator("a-"+appId, userId, appId, "display");
		display.setGatewayDeviceId(mobile.getId());
		display.setLatency(3.0);
		return new NetworkDevices(sensor,display,mobile,gateway,proxy,cloud);
	}
	
	private static FogDevice createFogDevice(String nodeName, long mips,
			int ram, long upBw, long downBw, int level, double ratePerMips, double busyPower, double idlePower) {
		List<Pe> peList = new ArrayList<Pe>();
		// 3. Create PEs and add these into a list.
		peList.add(new Pe(0, new PeProvisionerOverbooking(mips))); // need to store Pe id and MIPS Rating

		int hostId = FogUtils.generateEntityId();
		long storage = 1000000; // host storage
		int bw = 10000;

		PowerHost host = new PowerHost(
				hostId,
				new RamProvisionerSimple(ram),
				new BwProvisionerOverbooking(bw),
				storage,
				peList,
				new StreamOperatorScheduler(peList),
				new FogLinearPowerModel(busyPower, idlePower)
			);

		List<Host> hostList = new ArrayList<Host>();
		hostList.add(host);

		String arch = "x86"; // system architecture
		String os = "Linux"; // operating system
		String vmm = "Xen";
		double time_zone = 10.0; // time zone this resource located
		double cost = 3.0; // the cost of using processing in this resource
		double costPerMem = 0.05; // the cost of using memory in this resource
		double costPerStorage = 0.001; // the cost of using storage in this
										// resource
		double costPerBw = 0.0; // the cost of using bw in this resource
		LinkedList<Storage> storageList = new LinkedList<Storage>(); // we are not adding SAN
													// devices by now

		FogDeviceCharacteristics characteristics = new FogDeviceCharacteristics(
				arch, os, vmm, host, time_zone, cost, costPerMem,
				costPerStorage, costPerBw);

		FogDevice fogdevice = null;
		try {
			fogdevice = new FogDevice(nodeName, characteristics, 
					new AppModuleAllocationPolicy(hostList), storageList, 10, upBw, downBw, 0, ratePerMips);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		fogdevice.setLevel(level);
		return fogdevice;
	}
	@SuppressWarnings({"serial" })
	public static Application createApplication(String appId, int userId){
		Application application = Application.createApplication(appId, userId);
		application.addAppModule("Module1", 1000,250,700,false);
		application.addAppModule("Module2", 4000,500,1500,false);
		application.addAppModule("Module3", 2000,1000,2400,false);
		application.addAppModule("Module4", 2000,300,1700,true);
		application.addAppModule("Module5", 6000,2000,4000,false);
		application.addAppModule("Module6", 8000,5000,1700,false);
		
		application.addAppEdge("sensore", "Module1", 3000, 500, "sensore", Tuple.UP, AppEdge.SENSOR);
		application.addAppEdge("Module1", "Module2", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
		application.addAppEdge("Module2", "Module3", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
		application.addAppEdge("Module3", "Module4", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
		application.addAppEdge("Module4", "Module5", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
		application.addAppEdge("Module5", "Module6", 3500, 500, "dati", Tuple.UP, AppEdge.MODULE);
		
		application.addAppEdge("Module1", "Module6", 3500, 500, "dati", Tuple.DOWN, AppEdge.MODULE);
		application.addAppEdge("Module1", "display", 3500, 500, "output", Tuple.DOWN, AppEdge.ACTUATOR);
		
		application.addTupleMapping("Module1", "sensore", "dati", new FractionalSelectivity(1.0));
		application.addTupleMapping("Module2", "dati", "dati", new FractionalSelectivity(1.0));
		application.addTupleMapping("Module3", "dati", "dati", new FractionalSelectivity(1.0));
		application.addTupleMapping("Module4", "dati", "dati", new FractionalSelectivity(1.0));
		application.addTupleMapping("Module5", "dati", "dati", new FractionalSelectivity(1.0));
		application.addTupleMapping("Module6", "dati", "dati", new FractionalSelectivity(1.0));
		application.addTupleMapping("Module1", "dati", "output", new FractionalSelectivity(1.0));
		
//		
		final AppLoop loop1 = new AppLoop(new ArrayList<String>(){{add("Module1");add("Module2");add("Module3");add("Module4");add("Module5");add("Module6");}});
		List<AppLoop> loops = new ArrayList<AppLoop>(){{add(loop1);}};
//		
		application.setLoops(loops);
		return application;
	}
}
