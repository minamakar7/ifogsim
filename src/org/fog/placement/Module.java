package org.fog.placement;

public class Module {
	private String nome;
	private int livello;
	public Module(String nome,int livello) {
		this.nome = nome;
		this.livello = livello;
	}
	
	public String getNome(){
		return nome;
	}
	public int getLivello() {
		return livello;
	}

}
