package org.fog.placement;

import static java.util.Collections.reverseOrder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;
import org.cloudbus.cloudsim.Host;
import org.fog.application.AppEdge;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.entities.FogDevice;
import org.fog.test.perfeval.NetworkDevices;

public class LAPH2 extends ModulePlacement{
	private NetworkDevices struct;
	private ModuleMapping moduleMapping;
	private double[][] latency;
	private Map<Integer,List<Integer>> idAppToDevice = new HashMap<>(); 
	private int count = -1;
	private FileWriter changeLog;
	private int raw = 0;
	public LAPH2(NetworkDevices struct, Application application, ModuleMapping moduleMapping,double[][] latency) throws IOException {
		setMyStruct(struct);
//		this.setFogDevices(struct.getDevices());
		setApplications(new ArrayList<Application>());
		this.setApplication(application);
		this.setModuleMapping(moduleMapping);
		this.setModuleToDeviceMap(new HashMap<String, List<Integer>>());
		this.setDeviceToModuleMap(new HashMap<Integer, List<AppModule>>());
		setSensibleModule(application);
//		application.getModules().sort(Comparator.comparingInt(AppModule::getSensible).reversed());
		application.getModules().sort(null);
		this.latency = latency;
//		computeLatency(struct);
		stampaMatrice();
		changeLog = new FileWriter("file.txt");
		changeLog.write("ho cominciato\n");
	     
	}
	
	public void SpazioDispositi() {
		for(FogDevice fog : struct.getDevices().values()) {
			Host host = fog.getHost();
			System.out.println(fog.getId() + " storage: " + host.getStorage() + " mips: " + host.getAvailableMips()
			+ " Ram " + host.getRamProvisioner().getAvailableRam() + " BW: "+ host.getBwProvisioner().getAvailableBw());
		}
	}
	
	public void setMyStruct(NetworkDevices struct) {
		this.struct = struct;
	}
	public NetworkDevices getMyStruct() {
		return struct;
	}
	//doveva fare il calcolo delle latenze 
	public void computeLatency(NetworkDevices struct) {
		FogDevice fog = getDeviceById(struct.getSensore().getGatewayDeviceId());
		double temp = fog.getUplinkLatency();
		while(fog != null) {
			for (int i=0; i<fog.getId();i++) {
				if (fog.getId() > 0 ) {
					this.latency[i][fog.getId()] = this.latency[i][fog.getId()-1] + temp;
					this.latency[fog.getId()][i] = this.latency[fog.getId()-1][i] + temp;
				}
				this.latency[fog.getId()][fog.getId()] = 0;
			}
			temp = fog.getUplinkLatency();
			fog = fog.getParentId() != -1 ? getDeviceById(fog.getParentId()) : null;
		}
		stampaMatrice();
		
	}
	public void stampaMatrice() {
		System.out.print("[");
		for (int i = 0; i< this.latency.length; i++) {
			System.out.print(Arrays.toString(this.latency[i]));
			if (i != this.latency.length -1) 
				System.out.print(", ");
		}
		System.out.println();
	}
	
	public ModuleMapping getModuleMapping() {
		return moduleMapping;
	}
	public void setModuleMapping(ModuleMapping moduleMapping) {
		this.moduleMapping = moduleMapping;
	}
	
	public void taskControl() {
		int i = 0;
		while(i < getApplications().size()) {
			Application app = getApplications().get(i);
				i++;
				setRemainTimeTask(app);
		}
	}
	
	
	private void setRemainTimeTask(Application app) {
		app.setTaskTime(app.getTaskTime() - 1);
		if (app.getTaskTime() == 0)
			try {
				removeApp(app);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	private void removeModule(AppModule module) {
		FogDevice device = struct.getDevices().get(module.getDevicePlace());
//		System.out.println("Rimuovo " + module + " da " + device);
		device.getVmAllocationPolicy().deallocateHostForVm(module);
//		System.out.println("rimuovo device id:"+ device.getId() + " - module name: " + module.getName() +" place:" +module.getDevicePlace()+ " \n");
//		System.out.println(getDeviceToModuleMap() + "  " + device.getId() + " " + getDeviceToModuleMap().get(device.getId()));
		getDeviceToModuleMap().get(device.getId()).remove(module);
//		System.out.println("Rimosso " + module.getName() + " con id " + device.getId() +"!");
//		System.out.println("Mostro moduli restanti: " + getDeviceToModuleMap().get(device.getId()));
//		System.out.println("1) Device to module map:" + getDeviceToModuleMap());
		getModuleToDeviceMap().get(module.getName()).remove(Integer.valueOf(device.getId()));
//		System.out.println("2) Device to module map:" + getDeviceToModuleMap());
		if (getModuleToDeviceMap().get(module.getName()).size() == 0) getModuleToDeviceMap().remove(module.getName());
//		System.out.println("3) Device to module map:" + getDeviceToModuleMap());
	}
	private void removeApp(Application app) throws IOException {
		
//			System.out.println("lista iniziale "+ getDeviceToModuleMap()+ "\n");
//			System.out.println("rimuovo app "+ app.toString()+ "\n");
		
//		System.out.println("rimuovo app"+ app.toString());
		for (AppModule module: app.getModules()) {
			
			removeModule(module);
		}
//		System.out.println("lista finale "+ getDeviceToModuleMap()+ "\n");
		getApplications().remove(app);
	}

	@Override
	public void mapModules() {
		getApplications().add(this.getApplication());
		count++;
		List<Integer> fogPlace = new ArrayList<>();
		List<AppModule> toPlace = new ArrayList<>(this.getApplication().getModules());
		//prendo i moduli con un certo dispositivo richiesto
		for (AppModule m: this.getApplication().getModules()) {
			if (m.getIdDevice() != -1) {
				FogDevice f = struct.getDevices().get(m.getIdDevice());
				if (canInsert(m,f) && createModuleInstanceOnDevice(toPlace.get(0), f)) {
					fogPlace.add(f.getId());
					idAppToDevice.put(count, fogPlace);
					toPlace.remove(0);
					
					}
				else {
					System.out.println("Non e' stato possibile inserire il modulo nel dispositivo richiesto "+ m.getName());
					return;
				}
				
			}
		}
		// prendere il dispositivo piu vicino
//		FogDevice nearSensor = struct.getDevices().get(this.struct.getSensore().getGatewayDeviceId());
		List<FogDevice> mobileDevice = struct.getDevices().values().stream().filter(x-> x.getLevel() == 3).collect(Collectors.toList());
		FogDevice randomFog = mobileDevice.get((int)(Math.random() * mobileDevice.size()));
		//inserire quello piu sensibile
		FogDevice fog = randomFog;
		FogDevice lastDevice = randomFog;
		double latency = 0;
		if (toPlace.size() > 0 && toPlace.get(0).getSensible() == true) {
			Pair<FogDevice, Double> mostSensitive = insertMostSensitive(toPlace.get(0), fog);
			if (mostSensitive == null) return;
			lastDevice = mostSensitive.getFirst();
			latency += mostSensitive.getSecond();
		}

		while(!toPlace.isEmpty()) {
			AppModule mod = toPlace.get(0);
//			if (this.getModuleToDeviceMap().containsKey(mod.getName())) {
//				FogDevice fogModule = struct.getDevices().get(getModuleToDeviceMap().get(mod.getName()).get(0));
////				if (canInsert(mod, fogModule)) {
////					createModuleInstanceOnDevice(mod, fogModule);
//////					System.out.println("sssssssssssss"+ fogModule.getId());
////				}
////				else{
//					OptimizePlacement(mod, randomFog);
////				}
//				toPlace.remove(0);
//			}
//			else {
				Pair<FogDevice, Double> f = insertModule(lastDevice, mod);
				lastDevice = f.getFirst();
				if (fog != null) {
					createModuleInstanceOnDevice(mod, lastDevice);
					latency += f.getSecond();
//					System.out.println("lasssssssssssssssssst"+ lastDevice.getId());
				}
				toPlace.remove(0);
			
		}
		try {
			calcolaLatenza();
			System.out.println(getDeviceToModuleMap());
//			System.out.println(getApplications());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Pair<FogDevice,Double> insertMostSensitive(AppModule module, FogDevice nearSensor){
		//finche non viene inserito
		boolean done = false;
		double latency = 0;
		if (this.getModuleToDeviceMap().containsKey(module.getName())) 
			OptimizePlacement(module, getDeviceById(getModuleToDeviceMap().get(module.getName()).get(0)));
		HashSet<Integer> notSpace = new HashSet<>();
		while (!done) {
			if (nearSensor != null) {
				latency += this.latency[0][nearSensor.getId()];
				if (canInsert(module, nearSensor) ) {
					createModuleInstanceOnDevice(module, nearSensor);
					done = true;
//					System.out.println("seeeeenseeeeeeeeee "+ nearSensor.getId());
//					toPlace.remove(toPlace.get(0));
				}
				else {
					notSpace.add(nearSensor.getId());
					int min = trovaMin(this.latency[nearSensor.getId()],notSpace);
					nearSensor = getFogDeviceById(min);
				}
			}
			else {
				System.out.println("Si e' verificato un errore durante l'inserimento");
				return null;
			}
		}
		return new Pair<FogDevice, Double>(nearSensor,latency);
	}
	public Pair<FogDevice,Double> insertModule(FogDevice lastDevice, AppModule module) {
		double latenza = 0;
		FogDevice fog = null;
		boolean canPlace = false;
		for (int i = 0; i < this.latency.length; i++) {
			FogDevice temp =  struct.getDevices().get(i);
			if (canInsert(module, temp)) {
				if (!canPlace) {
					latenza = this.latency[lastDevice.getId()][i];
					fog = temp;
					canPlace = true;
				}
				else if (latenza > this.latency[lastDevice.getId()][i]) {
					latenza = this.latency[lastDevice.getId()][i];
					fog = temp;
				}
				
			}
		}
		return new Pair<FogDevice,Double>(fog,latenza);
	}
	public boolean canInsert(AppModule module,FogDevice fog) {
		Host host = fog.getHost();
		return module.getSize() <= host.getStorage() && module.getMips() <= host.getAvailableMips() && 
				module.getRam() <= host.getRamProvisioner().getAvailableRam() && module.getBw() <= host.getBwProvisioner().getAvailableBw();
	}
	//togliendo un certo modulo ho spazio?
	public boolean spaceForModule(AppModule toBeReplace, FogDevice device, AppModule toBeAdd) {
	Host host = device.getHost();
	return toBeAdd.getSize() <= host.getStorage() + toBeReplace.getSize() &&
			toBeAdd.getMips() <= host.getAvailableMips() + toBeReplace.getMips() && 
			toBeAdd.getRam() <= host.getRamProvisioner().getAvailableRam() + toBeReplace.getRam() && 
			toBeAdd.getBw() <= host.getBwProvisioner().getAvailableBw() + toBeReplace.getBw();
	} 
	// l'ho ultilizzavo all'inizio per trovare l'interlatenza tra due nodi
	public double interLatency(FogDevice first, FogDevice last,FogDevice current) {
		boolean boolLast = false;
		boolean boolCurrent = false;
		double latencyLast = 0;
		double latencyCurrent = 0;
		while(!boolLast || !boolCurrent) {
			if (!boolLast) {
				latencyLast += first.getUplinkLatency();
				if (first.getId() == last.getId())
					boolLast = true;
			}
			if (!boolCurrent) {
				latencyCurrent += first.getUplinkLatency();
				if (first.getId() == last.getId())
					boolCurrent = true;
			}
			first = getFogDeviceById(first.getParentId());
		}
		return Math.abs(latencyCurrent - latencyLast);
	}
	
	//ritorna una lista di vicini che non conterra' un figlio di livello sottostante
	public List<Pair<Integer,Double>> sortNeighbor(FogDevice fog, Map<Integer,Double> neighbors){
		List<Pair<Integer,Double>> siblings = new ArrayList<>();
		
		//System.out.println(neighbors);
		for (Integer neighbor: neighbors.keySet()) {
			//se � vicino o padre ma non figlio
			if (fog.getLevel() >= getMyStruct().getDevices().get(neighbor).getLevel()) 
				siblings.add(new Pair<Integer,Double>(neighbor,neighbors.get(neighbor)));
		}
		siblings.sort(reverseOrder(Comparator.comparing(Pair::getSecond)));
//		System.out.println(fog.getName()+ ": "+siblings);
		return siblings;
	}
	
	public void OptimizePlacement(AppModule module, FogDevice device){
		AppModule toBePlaced = module;
		// se c'e' almeno un istanza del modulo nel device
		if (this.getDeviceToModuleMap().containsKey(device.getId())) {
			for (AppModule m : getDeviceToModuleMap().get(device.getId())) {
				// se il modulo non e' sensibile ovvero puo' essere spostato e togliendo un quel modulo si puo' ottenere spazio
				if(m.getIdDevice() == -1 && m.getLivSensible() < 2 && 
						 spaceForModule(m, device, module)) {
					//module.getIdModule() < m.getIdModule() &&  
					toBePlaced = m;
					removeModule(m);
//					device.getVmAllocationPolicy().deallocateHostForVm(m);
//					getDeviceToModuleMap().get(device.getId()).remove(m);
////					System.out.println("faccio Optimize del modulo "+ m + " mappa:"+ getDeviceToModuleMap());
//					getModuleToDeviceMap().get(m.getName()).remove(Integer.valueOf(device.getId()));
					createModuleInstanceOnDevice(module,device);
//					System.out.println("Tolgo "+m+ " metto al posto suo "+ module + " in device "+ device.getId());
					break;
				}
			}
		}
		boolean placed = false;
		Set<Integer> visited = new HashSet<>();
		while(!placed) {
			visited.add(device.getId());
			List<Pair<Integer,Double>> siblings = sortNeighbor(device, device.getNeighbors());
			//mi scorro da quelli piu' costosi per portarli da me
			for (Pair<Integer, Double> sibling : siblings) {
				if (!visited.contains(sibling.getFirst())) {
					FogDevice neigh = struct.getDevices().get(sibling.getFirst());
					if (neigh == null) continue;
					if (this.getDeviceToModuleMap().containsKey(neigh.getId())) {
						List<AppModule> lista = new ArrayList<>(getDeviceToModuleMap().get(neigh.getId()));
						for (AppModule m : lista) {
							//System.out.println("Ho bhoooooooooooooooooooS ");
							if (canInsert(m, device)) {
								removeModule(m);
//								neigh.getVmAllocationPolicy().deallocateHostForVm(m);
//								getDeviceToModuleMap().get(neigh.getId()).remove(m);
//								getModuleToDeviceMap().get(m.getName()).remove(Integer.valueOf(neigh.getId()));
								createModuleInstanceOnDevice(m,device);
								
//								System.out.println("Ho bho "+ m.getId());
//								System.out.println("Inserisco: "+ m+ " in device "+ device+ " con set place "+module.getDevicePlace());
							}
						}
					}
				}
			}
			siblings.sort(Comparator.comparing(Pair::getSecond));
			for (Pair<Integer, Double> sibling : siblings) {
				if (!visited.contains(sibling.getFirst())) {
					FogDevice parent = struct.getDevices().get(sibling.getFirst());
					if (canInsert(toBePlaced, parent)) {
						createModuleInstanceOnDevice(toBePlaced,parent);
						placed = true;
//						System.out.println(toBePlaced.getDevicePlace()+ " sono il parent del modulo "+ toBePlaced);
//						System.out.println("Inserisco "+ toBePlaced + "in device parent "+ parent.getId()+ "con set place "+ toBePlaced.getDevicePlace());
						break;
					}
				}
			}
			
			if (placed == false) {
				try {
					
					device = struct.getDevices().get(siblings.stream().
							filter(x-> !visited.contains(x.getFirst())).findFirst().get().getFirst());
				}
				catch (Exception e) {
//					System.out.println(toBePlaced + " "+ "bw: " + toBePlaced.getBw()+ " ram: "+ toBePlaced.getRam()+ ""
//							+ " mips: "+ toBePlaced.getMips()+ " storage"+ toBePlaced.getSize());
					 System.exit(-1);
					 
				}
				
			}
		}
	}
	public void setSensibleModule(Application application) {
		for (AppEdge edge: application.getEdges()) {
			if (edge.getEdgeType() == 1) {
				AppModule module = application.getModuleByName(edge.getDestination());
				if (module.getSensible())
					module.setLivSensible(2);
				
			}
			else if (edge.getEdgeType() == 3) { 
				AppModule module1 = application.getModuleByName(edge.getSource());
				AppModule module2 = application.getModuleByName(edge.getDestination());
				if (module1.getLivSensible() == 0) module1.setLivSensible(1);
				if (module2.getLivSensible() == 0) module2.setLivSensible(1);
			}
			
		}
	}
	
	public int trovaMin(double[] latency,Set<Integer> notSpace) {
		double min = latency[0];
		int pos  = 0;
		for (int i=1; i< latency.length; i++) 
			if (!notSpace.contains(i) && min > latency[i]) {
				min = latency[i];
				pos = i;
			}
		return pos;
	}
	public void calcolaLatenza() throws IOException {
		Map<String,Double> latenze = new HashMap<>();
		for (Application app : getApplications()) {
			int lastDevice = 0;
			for (int i = 0; i < app.getModules().size(); i++) {
//				System.out.println(module.getDevicePlace());
				if (i == 0) {
					latenze.put(app.getAppId(), 0.0);
					lastDevice = struct.getSensore().getGatewayDeviceId();
				}
				latenze.put(app.getAppId(),latenze.get(app.getAppId()) + 
						this.latency[lastDevice][app.getModules().get(i).getDevicePlace()]);
				lastDevice = app.getModules().get(i).getDevicePlace();
			}
		}
		//changeLog.write(Arrays.toString(latency[struct.getSensore().getGatewayDeviceId()]) + " \n"+ "device : "+getDeviceToModuleMap()+ "\n"+ "Latenza: "+ latenze+ "\n");
//		System.out.println(Arrays.toString(latency[struct.getSensore().getGatewayDeviceId()]));
//		System.out.println("device : "+getDeviceToModuleMap());
		System.out.println(latenze);
	}
	
}

//boolean placed = false;
//fog = nearSensor;
//done = false;
//while (!done) {
//	if (fog != null) {
//		if (canInsert(mod, fog)) {
//			done = true;
//			if (!placed) {
//				System.out.println("sono nel if del "+ mod.getName()+" " + this.latency[lastDevice.getId()][fog.getId()]);
//				//tempLatency = interLatency(nearSensor, lastDevice, fog) + latency;
//				tempLatency = latency + this.latency[lastDevice.getId()][fog.getId()];
//				tempDevice = fog;
//				placed = true;
//			}
//			else {
//				System.out.println("sono nel else" + this.latency[lastDevice.getId()][fog.getId()]);
//				double overalCom = latency + this.latency[lastDevice.getId()][fog.getId()] + latency;
//				if (tempLatency > overalCom) {
//					tempLatency = overalCom;
//					tempDevice = fog;
//				}
//			}
//		}
//		else fog = getDeviceById(fog.getParentId());
//
//	}
//}




//for (AppLoop loop: this.getApplication().getLoops()) {
//double latency = 0;
//List<String> modulesName = loop.getModules();
//for (String moduleName: modulesName) {
//	if (!alreadyPlaced.contains(moduleName)) {
//		AppModule module = getApplication().getModuleByName(moduleName);
//		if (this.getModuleToDeviceMap().containsKey(module.getName()))
//			OptimizePlacement(module, getDeviceById(getModuleToDeviceMap().get(module.getName()).get(0)));
//		else {
//			Pair<FogDevice, Double> f = insertModule(lastDevice, module);
//			if (f.getFirst() != null) {
//				lastDevice = f.getFirst();
//				createModuleInstanceOnDevice(module, lastDevice);
//				latency += f.getSecond();
//				alreadyPlaced.add(module.getName());
//			}
//			else {
//				System.out.println("problema "+ module.getName());
//				return;
//			}
//		}
//	}
//	else {
//		latency += tempLatency;
//		System.out.println(moduleName+" gi� piazzato");
//	}
//}
//taskLatency.add(latency);
//}