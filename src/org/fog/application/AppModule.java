package org.fog.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.util.Pair;
import org.cloudbus.cloudsim.CloudletScheduler;
import org.cloudbus.cloudsim.power.PowerVm;
import org.fog.application.selectivity.SelectivityModel;
import org.fog.scheduler.TupleScheduler;
import org.fog.utils.FogUtils;

/**
 * Class representing an application module, the processing elements of the application model of iFogSim.
 * @author Harshit Gupta
 *
 */
public class AppModule extends PowerVm implements Comparable<AppModule>{

	private String name;
	private String appId;
	private Map<Pair<String, String>, SelectivityModel> selectivityMap;
	private boolean sensible = false;
	private int livSensible = 0;
	private int idDevice = -1; //where the user want to stay the module place
	private int idModule = -1;
	private int numApp = 0;
	private int devicePlace;
	/**
	 * A map from the AppModules sending tuples UP to this module to their instance IDs.
	 * If a new instance ID is detected, the number of instances is incremented.  
	 */
	private Map<String, List<Integer>> downInstanceIdsMaps;
	
	/**
	 * Number of instances of this module
	 */
	private int numInstances;
	
	/**
	 * Mapping from tupleType emitted by this AppModule to Actuators subscribing to that tupleType
	 */
	private Map<String, List<Integer>> actuatorSubscriptions;
	
	public AppModule(
			int id,
			String name,
			String appId,
			int userId,
			double mips,
			int ram,
			long bw,
			long size,
			String vmm,
			CloudletScheduler cloudletScheduler,
			Map<Pair<String, String>, SelectivityModel> selectivityMap) {
		super(id, userId, mips, 1, ram, bw, size, 1, vmm, cloudletScheduler, 300);
		setName(name);
		setId(id);
		setAppId(appId);
		setUserId(userId);
		setUid(getUid(userId, id));
		setMips(mips);
		setNumberOfPes(1);
		setRam(ram);
		setBw(bw);
		setSize(size);
		setVmm(vmm);
		setCloudletScheduler(cloudletScheduler);
		setInMigration(false);
		setBeingInstantiated(true);
		setCurrentAllocatedBw(0);
		setCurrentAllocatedMips(null);
		setCurrentAllocatedRam(0);
		setCurrentAllocatedSize(0);
		setSelectivityMap(selectivityMap);
		setActuatorSubscriptions(new HashMap<String, List<Integer>>());
		setNumInstances(0);
		setDownInstanceIdsMaps(new HashMap<String, List<Integer>>());
	}
	public AppModule(AppModule operator) {
		super(FogUtils.generateEntityId(), operator.getUserId(), operator.getMips(), 1, operator.getRam(), operator.getBw(), operator.getSize(), 1, operator.getVmm(), new TupleScheduler(operator.getMips(), 1), operator.getSchedulingInterval());
		setName(operator.getName());
		setAppId(operator.getAppId());
		setInMigration(false);
		setBeingInstantiated(true);
		setCurrentAllocatedBw(0);
		setCurrentAllocatedMips(null);
		setCurrentAllocatedRam(0);
		setCurrentAllocatedSize(0);
		setSelectivityMap(operator.getSelectivityMap());
		setDownInstanceIdsMaps(new HashMap<String, List<Integer>>());
		setNumApp(operator.getNumApp()+1);
		devicePlace = operator.devicePlace;
	}
	
	public void subscribeActuator(int id, String tuplyType){
		if(!getActuatorSubscriptions().containsKey(tuplyType))
			getActuatorSubscriptions().put(tuplyType, new ArrayList<Integer>());
		getActuatorSubscriptions().get(tuplyType).add(id);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<Pair<String, String>, SelectivityModel> getSelectivityMap() {
		return selectivityMap;
	}
	public void setSelectivityMap(Map<Pair<String, String>, SelectivityModel> selectivityMap) {
		this.selectivityMap = selectivityMap;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public Map<String, List<Integer>> getActuatorSubscriptions() {
		return actuatorSubscriptions;
	}
	public void setActuatorSubscriptions(Map<String, List<Integer>> actuatorSubscriptions) {
		this.actuatorSubscriptions = actuatorSubscriptions;
	}
	public Map<String, List<Integer>> getDownInstanceIdsMaps() {
		return downInstanceIdsMaps;
	}
	public void setDownInstanceIdsMaps(Map<String, List<Integer>> downInstanceIdsMaps) {
		this.downInstanceIdsMaps = downInstanceIdsMaps;
	}
	public int getNumInstances() {
		return numInstances;
	}
	public void setNumInstances(int numInstances) {
		this.numInstances = numInstances;
	}
	public void setLivSensible(int sens) {
		this.livSensible = sens;
	}
	public void setSensible(boolean sens) {
		this.sensible = sens;
	}
	public boolean getSensible() {
		return this.sensible;
	}
	public int getLivSensible() {
		return this.livSensible;
	}
	public void setIdDevice(int idDevice) {
		this.idDevice = idDevice;
	}
	public int getIdDevice() {
		return this.idDevice;
	}
	public void setIdModule(int id) {
		this.idModule = id;
	}
	public int getIdModule() {
		return idModule;
	}
	
	public int getNumApp() {
		return numApp;
	}
	
	public void setNumApp(int num) {
		numApp = num;
	}
	public void setDevicePlace(int place) {
		this.devicePlace = place;
	}
	
	public int getDevicePlace() {
		return devicePlace;
	}
	@Override
	public String toString() {
		return getName()+ " place:"+ getDevicePlace();
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj == null) return false;
		if (obj instanceof AppModule) {
			AppModule mod = (AppModule) obj;
			return mod.getAppId() == getAppId() && mod.getName().equals(getName());
		}
		return false;
	}
	@Override
	public int compareTo(AppModule o) {
		if (this.getIdDevice() !=-1 && this.getSensible() && !o.getSensible()) return -1;
		if (this.getIdDevice() != -1 && o.getIdDevice() == -1) return -1;
		if (this.getIdDevice() != -1 && o.getIdDevice() != -1) return this.getIdDevice() - o.getIdDevice();
		if (o.getIdDevice() != -1 && this.getIdDevice() == -1) return 1;
		if (this.getLivSensible() > o.getLivSensible()) return -1;
		if (this.getLivSensible() < o.getLivSensible()) return 1;
		return 0;
	}
}
